<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Manual extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper(array('url','date'));
        $this->load->model(array('Select_db','Insert_db','Update_db','Delete_db'));
        $this->load->library(array('initlib','session','datatables','email'));
        $this->load->database();
        
    }
    function index(){
        $data['title'] = 'Aplikasi P3M : Manual Aplikasi';
        $data['module']='mod_manual';
        $this->load->view('handling/main_view',$data);
    }
    
    function post_user_info(){
        
    }
}