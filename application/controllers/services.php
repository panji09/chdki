<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Services extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper(array('url'));
		$this->load->model(array('admin_handling/pengaduan_db' ,'admin_handling/region_db', 'select_db'));
		$this->load->library(array('email','initlib','session'));
		$this->load->database();
	}
	function get_kabkota(){
		$data['provinsi_id']=$this->input->post('provinsi_id');
		$kabkota=$this->region_db->kabkota($data);
		$this->output->set_output(json_encode($kabkota->result()));
	}
	function get_kecamatan(){
		$data['kabkota_id']=$this->input->post('kabkota_id');
		$kecamatan=$this->region_db->kecamatan($data);
		$this->output->set_output(json_encode($kecamatan->result()));
	}
	function get_desa(){
		$data['KdKec']=$this->input->post('KdKec');
		$desa=$this->select_db->desa($data);
		$this->output->set_output(json_encode($desa->result()));
	}
	function post_token_email($email,$id){
		$email=base64_decode($email);
		$user=$this->select_db->t_user('by_id',array('id' => $id))->row();
		$data['nama'] = $user->username;
		$data['kode'] = $user->token;
		
		//sent email
		/*
		$this->email->from('bos@kemdikbud.go.id', 'Bantuan Operasional Sekolah');
		$this->email->to($email);
		$this->email->subject('Kode Registrasi P3M BOS');
		$msg=$this->load->view('home/send_email_token',$data,true);
		$this->email->message($msg);
		$this->email->send();
		*/
		$msg=$this->load->view('home/send_email_token',$data,true);
		$data_email=array(
		  'from' => 'bos@kemdikbud.go.id|BOS kemdikbud',
		  'to' => $email,
		  'subject' => 'Kode Registrasi P3M BOS',
		  'body' => $msg
		);
		$this->Insert_db->send_email($data_email);
	}
	
	function post_bantuan($from,$name,$pesan){
		$from=base64_decode($from);
		$name=base64_decode($name);
		$pesan=base64_decode($pesan);
		
		$from=explode(",",$from);
		
		//sent email
		$from_ask=$from[0].'|'.$name;
		
		unset($from[0]);
		$cc='panji09@gmail.com';
		
		$from=implode(",",$from);
		if($from !='')
			$cc=$cc.','.$from;
		
		/*
		$this->email->to('bos.kemdikbud@gmail.com');
		$this->email->cc($cc);
		$this->email->subject('[ASK] '.$name.' P3M BOS');
		//$msg=$this->load->view('home/send_email_token',$data,true);
		$this->email->message($pesan);
		$this->email->send();
		*/
		$data_email=array(
		  'from' => $from_ask,
		  'to' => 'bos.kemdikbud@gmail.com, bos@kemdikbud.go.id',
		  'cc' => $cc,
		  'subject' => '[ASK] '.$name.' P3M BOS',
		  'body' => $msg
		);
		$this->Insert_db->send_email($data_email);
	}
	
	function get_table_faq($id_kat){
		$faq=$this->select_db->faq('by_kategori',array('id_kategori' => $id_kat));
		if($id_kat=="tambah"){
			echo '
			<tr id="a1">
				<td valign="top">1.</td>
				<td valign="top">Pertanyaan:</td>
				<td><textarea name="tanya[]" cols="100" rows="3" id="tanya1"></textarea> <input type="hidden" name="id_faq[]" value="" id="id_faq1" /> </td>
				<td valign="top"><input type="button" value="hapus" class="hapus styled-button-2" id="1"/></td>
			</tr>
			<tr id="q1">
			  <td valign="top">&nbsp;</td>
				<td valign="top">Jawaban:</td>
				<td><textarea name="jawab[]" cols="100" rows="5" id="jawab1"></textarea></td>
				<td>&nbsp;</td>
			</tr>
			';
		}else{
			$i=1;
			foreach($faq->result() as $row){
				echo '
				<tr id="a'.$i.'">
					<td valign="top">'.$i.'.</td>
					<td valign="top">Pertanyaan:</td>
					<td><textarea name="tanya[]" cols="100" rows="3" id="tanya'.$i.'">'.$row->tanya.'</textarea><input type="hidden" name="id_faq[]" value="'.$row->id.'" id="id_faq'.$i.'"/></td>
					<td valign="top"><input type="button" value="hapus" class="hapus styled-button-2" id="'.$i.'"/></td>
				</tr>
				<tr id="q'.$i.'">
				  <td valign="top">&nbsp;</td>
					<td valign="top">Jawaban:</td>
					<td><textarea name="jawab[]" cols="100" rows="5" id="tanya'.$i.'">'.$row->jawab.'</textarea></td>
					<td>&nbsp;</td>
				</tr>
				';
				$i++;
			}
		}
	}
	
	function delete_faq($id){
		//$this->initlib->allow_ip(array('127.0.0.1'));
		$this->initlib->cek_session_admin();
		$this->Delete_db->faq(array('id' => $id));
	}
	
	function faq($param=null,$id){
		if($param=='count'){
			echo $this->select_db->faq('by_kategori',array('id_kategori' => $id))->num_rows();
		}
	}
	function faq_kategori($param=null,$id){
		//$this->initlib->allow_ip(array('127.0.0.1'));
		$this->initlib->cek_session_admin();
		if($param=='delete'){
			$this->Delete_db->faq_kategori(array('id' => $id));
		}
	}
	
	function get_report($chart=null, $prov=null, $kabkota=null, $tahun=null){
		/*
			'r1' => 'Kategori',
			'r2' => 'Sumber Informasi',
			'r3' => 'Jumlah & Status',
			'r4' => 'Kategori & Status',
			'r5' => 'Pelaku & Status',
			'r6' => 'Status Provinsi',
			'r7' => 'Media'
		*/
		if(($chart=='' || $prov=='' || $kabkota=='' || $tahun=='') && ($chart==null || $prov==null || $kabkota==null || $tahun==null))
		$this->output->set_output('missing parameter');
		
		$data['prov']=$this->select_db->prov();
		$data['kabkota']=$this->select_db->kabkota(array('KdProv' => $prov));
		
		$data_param='';
		if($prov!='all'){
			$data_param['id_prov']=$prov;
		}
		if($kabkota!='all'){
			$data_param['id_kabkota']=$kabkota;
		}
		if($tahun!='all')
			$data_param['tahun']=$tahun;
		
		if($chart=='r1'){//kategori & status
			$report=$this->select_db->report_kategori(null,$data_param);
		}elseif($chart=='r2'){
			$report=$this->select_db->report_sumber_info(null,$data_param);
		}elseif($chart=='r3'){
			$report=$this->select_db->report_jumlah_status(null,$data_param);
		}elseif($chart=='r4'){
			$report=$this->select_db->report_kategori('by_status',$data_param);
		}elseif($chart=='r5'){
			$report=$this->select_db->report_pelaku(null,$data_param);
		}elseif($chart=='r6'){
			$report=$this->select_db->report_daerah(null,$data_param);
			//echo $this->db->last_query();
		}elseif($chart=='r7'){
			$report=$this->select_db->report_media(null,$data_param);
		}
		$this->output->set_header('Content-Type: application/json');
		$this->output->set_output(json_encode($report->result()));
	}
	
	function get_penyelesaian(){
	    $kategori_penyelesaian_id = $this->input->post('kategori_penyelesaian_id');
	    $penyelesaian = $this->select_db->penyelesaian(array('kategori_penyelesaian_id' => $kategori_penyelesaian_id));
	    //echo $this->db->last_query();
	    $this->output->set_output(json_encode($penyelesaian->result()));
	}
	
	function get_count_unapproved(){
	    $query = $this->pengaduan_db->get_all(array('approved' => 0));
	    echo $query->num_rows();
	}

}