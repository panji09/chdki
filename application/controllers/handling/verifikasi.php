<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Verifikasi extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper(array('url','date','init'));
        $this->load->model(array('select_db','admin_handling/region_db','admin_handling/pengaduan_db', 'admin_handling/respon_db', 'admin_handling/person_db','admin_handling/user_handling_db', 'admin_handling/user_admin_db'));
        $this->load->library(array('initlib','session','datatables','email'));
        $this->load->database();
        
	//$this->provinsi=31;
	$this->role_jenjang = $this->session->userdata('role_jenjang');
	$this->role_level = $this->session->userdata('role_level');
	$this->role_program = $this->session->userdata('role_program');
    
	$this->initlib->cek_session_handling();
	
	$this->user_handling = $this->session->userdata('user_handling');
	//print_r( $this->session->userdata('user_handling')->id);
	
	$this->program_id = 3;
    }
    function index(){
	//print_r($_SERVER); die();
        //print_r($this->role_level);
        $user_login=$this->session->userdata('handling');
        $data['title'] = 'Aplikasi P3M : Data Pengaduan';
        $data['module']='mod_verifikasi';
        
        //load ajax pengaduan/filter
        $data['link_ajax_pengaduan']=site_url('handling/verifikasi/ajax_pengaduan');
        //$data['pengaduan']=$this->select_db->handling_pengaduan();
        
        //print_r($data['pengaduan']->result()); echo $this->db->last_query(); die();
        $this->load->view('handling/main_view',$data);
    }
    
    function ajax_pengaduan(){
	$role_jenjang = $this->role_jenjang;
        $role_program = $this->role_program;
        $role_level = $this->role_level;
        
	$this->datatables->select('
	    a.id as id,
	    k.name as program,
	    h.name as kategori,
	    a.tanggal as tanggal,
	    concat(e.name,", ",d.name,", ",c.name) as lokasi,
	    a.deskripsi as deskripsi,
	    if(a.published = 1,"ya","tidak") as tampilkan,
	    if(a.approved = 1,"ya","tidak") as verifikasi,
	    a.id as action,
	    a.approved as approved
	',false);
	
	$this->datatables->from('pengaduan as a');
	$this->datatables->join('person as b','a.person_id = b.id','left');
	$this->datatables->join('kecamatan as c','a.kecamatan_id = c.id','left');
	$this->datatables->join('kabkota as d','a.kabkota_id = d.id','left');
	$this->datatables->join('provinsi as e','a.provinsi_id = e.id','left');
	$this->datatables->join('jenjang as f','a.jenjang_id = f.id','left');
	$this->datatables->join('sumber as g','a.sumber_id = g.id','left');
	$this->datatables->join('kategori as h','a.kategori_id = h.id','left');
	$this->datatables->join('level as i','a.level_id = i.id','left');
	$this->datatables->join('media as j','a.media_id = j.id','left');
	$this->datatables->join('program as k','a.program_id = k.id','left');

	$this->datatables->where('a.deleted',0);
	
	//role
	$this->datatables->where('a.level_id',$role_level->level_id);
        if($role_level->level_id==2){
	    $this->datatables->where('a.provinsi_id',$role_level->provinsi_id);
	}elseif($role_level->level_id==3){
	    $this->datatables->where('a.provinsi_id',$role_level->provinsi_id);
	    $this->datatables->where('a.kabkota_id',$role_level->kabkota_id);
	}
	$this->datatables->where('a.program_id IN ('.implode(",", $role_program).')');
        $this->datatables->where('(a.jenjang_id IN ('.implode(",", $role_jenjang).') OR a.jenjang_id is null)');
        //$this->datatables->or_where('a.jenjang_id is null');
        //end role
        
	$this->datatables->edit_column('deskripsi','<div class="expander">$1</div>','deskripsi');
	$this->datatables->edit_column('tanggal','$1','mysqldatetime_to_date(tanggal,"d/m/Y, H:i:s")');
	$this->datatables->edit_column(
	    'action',
	    '
	    <div class="btn-group btn-group-sm" style="min-width: 100px">
		<button style="$6" type="button" class="btn btn-default btn_edit" data-id=$1 data-approved=$4 data-published=$5 data-action="$2/$1" data-toggle="modal" data-target="#modal_approved">Terima</button>
		<button type="button" class="btn btn-default btn_delete" data-action="$3/$1" data-toggle="modal" data-target="#modal_delete_reason">Tolak</button>
	    </div>',
	    'id, site_url("handling/verifikasi/approved"), site_url("handling/verifikasi/delete"), verifikasi, tampilkan, display_approve(approved)'
	);
	echo $this->datatables->generate();
    }
    
    function load_pengaduan($id){
	$query = $this->pengaduan_db->get($id);
	
	if($query->num_rows()==1){
	    $pengaduan = $query->row();
	    $query = $this->pengaduan_db->get_param($pengaduan->id);
	    //print_r($query->result());
	    //$param = array();
	    foreach($query->result() as $row){
		$param[$row->key] = $row->value;
	    }
	    echo "<table class='table'>
		  <tbody>
		      <tr>
			  <td><label>Ditujukan Kepada</label></td>
			  <td>Tim BOS ".ucfirst($pengaduan->level)."</td>
		      </tr>
		      <tr>
			  <td><label>Daerah</label></td>
			  <td>".$pengaduan->provinsi.", ".$pengaduan->kabkota.", ".$pengaduan->kecamatan."</td>
		      </tr>
		      <tr>
			  <td><label>Lokasi</label></td>
			  <td>".($pengaduan->lokasi_id == 1 ? $pengaduan->jenjang.' '.ucfirst($pengaduan->status_sekolah).', '.$pengaduan->nama_lokasi : $pengaduan->nama_lokasi)."</td>
		      </tr>
		      <tr>
			  <td><label>Kategori</label></td>
			  <td>".$pengaduan->kategori."</td>
		      </tr>
		      <tr>
			  <td><label>Profesi</label></td>
			  <td>".($pengaduan->sumber_id == 9999 ? $pengaduan->sumber.'('.$pengaduan->sumber_lain.')' : $pengaduan->sumber)."</td>
		      </tr>
		      <tr>
			  <td><label>Nama</label></td>
			  <td>".$pengaduan->nama." (Tampilkan : ".($param['tampil_nama']==1 ? 'Ya' : 'Tidak').")</td>
		      </tr>
		      <tr>
			  <td><label>Email</label></td>
			  <td>".$pengaduan->email."</td>
		      </tr>
		      <tr>
			  <td><label>Telp</label></td>
			  <td>".$pengaduan->handphone." (Tampilkan : ".($param['tampil_telp']==1 ? 'Ya' : 'Tidak').")</td>
		      </tr>
		      <tr>
			  <td><label>Alamat</label></td>
			  <td>".$pengaduan->alamat_rumah." (Tampilkan : ".($param['tampil_alamat']==1 ? 'Ya' : 'Tidak').")</td>
		      </tr>
		      <tr>
			  <td><label>Deskripsi</label></td>
			  <td>".$pengaduan->deskripsi."</td>
		      </tr>
		      <tr>
			  <td>&nbsp</td>
			  <td><input type='checkbox' name='published' value='1' id='published'> Publikasi</td>
		      </tr>
		  </tbody>
	      </table>";
	}
    }
    
    function delete($pengaduan_id){
	$data_pengaduan = array(
	    'deleted' => 1,
	    'reason_deleted' => $this->input->post('reason_deleted')
	);
	$query = $this->pengaduan_db->get($pengaduan_id);
	if($this->pengaduan_db->save_pengaduan($pengaduan_id, $data_pengaduan)){
	    
	    if($query->num_rows() == 1){
		$row = $query->row();
		    
		$data_email = array(
		    'nama' => $row->nama,
		    'content' => array(
			'Ditujukan Kepada' => 'Tim Bos '.ucfirst($row->level),
			'Daerah' => $row->provinsi.', '.$row->kabkota.', '.$row->kecamatan,
			'Lokasi' => ($row->lokasi_id == 1 ? $row->jenjang.' '.$row->status_sekolah.', '.$row->nama_lokasi : $row->nama_lokasi),
			'Kategori' => $row->kategori,
			'Deskripsi' => $row->deskripsi
		    ),
		    'status' => 'Ditolak',
		    'reply' => $this->input->post('reason_deleted')
		);
		//bcc to admin
		$query = $this->user_admin_db->get_all();
		if($query->num_rows()){
		    $email_admin = array();
		    foreach($query->result() as $row_admin){
			$email_admin[] = $row_admin->email;
		    }
		    
		    if($email_admin){
			$email_admin = implode(',',$email_admin);
			$email_admin = explode(',',$email_admin);
		    }
		}
		$message = $this->load->view('email/status_pengaduan', $data_email,true);
		send_email($from=array('name'=>'P3M BOP dan KJP', 'email' => 'ch.emailer.system@gmail.com'), $to=array($row->email), $cc=array(), $bcc=$email_admin, $subject='P3M BOP dan KJP | Status Pengaduan', $message);
	    }
            $this->session->set_flashdata('success', array('status' => true, 'msg' => 'sukses'));
        }else{
            $this->session->set_flashdata('success', array('status' => false, 'msg' => 'gagal'));
        }
	redirect('handling/verifikasi');
    }
    
    function approved($pengaduan_id){
	$data_pengaduan = array(
	    'approved' => 1,
	    'published' => ($this->input->post('published') ? 1 : 0)
	);
	
	
	if($this->pengaduan_db->approved($pengaduan_id, $data_pengaduan)){
	    $query = $this->pengaduan_db->get($pengaduan_id);
	    if($query->num_rows() == 1){
		$row = $query->row();
		    
		$data_email = array(
		    'nama' => $row->nama,
		    'content' => array(
			'Kode Pengaduan' => $row->kode_pengaduan,
			'Publikasi' => ($row->published ? 'Ya' : 'Tidak'),
			'Verifikasi' => ($row->approved ? 'Ya' : 'Tidak'),
			'Ditujukan Kepada' => 'Tim Bos '.ucfirst($row->level),
			'Daerah' => $row->provinsi.', '.$row->kabkota.', '.$row->kecamatan,
			'Lokasi' => ($row->lokasi_id == 1 ? $row->jenjang.' '.$row->status_sekolah.', '.$row->nama_lokasi : $row->nama_lokasi),
			'Kategori' => $row->kategori,
			'Deskripsi' => $row->deskripsi
		    ),
		    'status' => 'Proses'
		);
		//bcc to admin
		$query = $this->user_admin_db->get_all();
		if($query->num_rows()){
		    $email_admin = array();
		    foreach($query->result() as $row_admin){
			$email_admin[] = $row_admin->email;
		    }
		    
		    if($email_admin){
			$email_admin = implode(',',$email_admin);
			$email_admin = explode(',',$email_admin);
		    }
		}
		$message = $this->load->view('email/status_pengaduan', $data_email,true);
		send_email($from=array('name'=>'P3M BOP dan KJP', 'email' => 'ch.emailer.system@gmail.com'), $to=array($row->email), $cc=array(), $bcc=$email_admin, $subject='P3M BOP dan KJP | Status Pengaduan', $message);
	    }
	    
            $this->session->set_flashdata('success', array('status' => true, 'msg' => 'sukses'));
        }else{
            $this->session->set_flashdata('success', array('status' => false, 'msg' => 'gagal'));
        }
	redirect('handling/verifikasi');
    }
    
    function template(){
        $this->load->view('handling/template_view');
    }
}

?>