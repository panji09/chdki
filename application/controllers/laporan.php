<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Laporan extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper(array('url','date'));
        $this->load->model(array('Select_db','Insert_db','Update_db','Delete_db'));
        $this->load->library(array('initlib','session','datatables','email'));
        $this->load->database();
        
    }
    
    function index(){
        $this->initlib->cek_session_handling();
        redirect('laporan/r1/all/all');
    }
    
    function post_laporan(){
        redirect('laporan/'.$this->input->post('kategori').'/'.$this->input->post('tahun').'/'.$this->input->post('triwulan'));
    }
    function r1($tahun=null, $triwulan=null){
        $this->initlib->cek_session_handling();
        
        $data['title'] = 'Aplikasi P3M : Laporan';
        $data['module']='mod_laporan';
        $data_param['jenjang']=$this->session->userdata('handling')->jenjang;
        
        switch($this->session->userdata('handling')->instansi){
            case 'PUSAT':
                
            break;
            case 'PROPINSI':    
                $data_param['id_prov']=$this->session->userdata('handling')->propinsi;
                
            break;
            case 'DAERAH':
                $data_param['id_kabkota']=$this->session->userdata('handling')->kabupaten;
            break;
        }
        
        if($tahun!='all')
            $data_param['tahun']=$tahun;
        
        if($triwulan!='all')
            $data_param['triwulan']=$triwulan;
            
        $data['report']=$this->Select_db->report_kategori(null,$data_param);
        
        $this->load->view('handling/main_view',$data);
    }
    
    function r2($tahun=null, $triwulan=null){
        $this->initlib->cek_session_handling();
        
        $data['title'] = 'Aplikasi P3M : Laporan';
        $data['module']='mod_laporan';
        
        $data_param['jenjang']=$this->session->userdata('handling')->jenjang;
        
        switch($this->session->userdata('handling')->instansi){
            case 'PUSAT':
                
            break;
            case 'PROPINSI':    
                $data_param['id_prov']=$this->session->userdata('handling')->propinsi;
                
            break;
            case 'DAERAH':
                $data_param['id_kabkota']=$this->session->userdata('handling')->kabupaten;
            break;
        }
        
        if($tahun!='all')
            $data_param['tahun']=$tahun;
        
        if($triwulan!='all')
            $data_param['triwulan']=$triwulan;
            
        $data['report']=$this->Select_db->report_sumber_info(null,$data_param);
        
        $this->load->view('handling/main_view',$data);
    }
    
    function r3($tahun=null, $triwulan=null){
        $this->initlib->cek_session_handling();
        
        $data['title'] = 'Aplikasi P3M : Laporan';
        $data['module']='mod_laporan';
        
        $data_param['jenjang']=$this->session->userdata('handling')->jenjang;
        
        switch($this->session->userdata('handling')->instansi){
            case 'PUSAT':
                
            break;
            case 'PROPINSI':    
                $data_param['id_prov']=$this->session->userdata('handling')->propinsi;
                
            break;
            case 'DAERAH':
                $data_param['id_kabkota']=$this->session->userdata('handling')->kabupaten;
            break;
        }
        
        if($tahun!='all')
            $data_param['tahun']=$tahun;
        
        if($triwulan!='all')
            $data_param['triwulan']=$triwulan;
            
        $data['report']=$this->Select_db->report_jumlah_status(null,$data_param);
        
        $this->load->view('handling/main_view',$data);
    }
    
    function r4($tahun=null, $triwulan=null){
        $this->initlib->cek_session_handling();
        
        $data['title'] = 'Aplikasi P3M : Laporan';
        $data['module']='mod_laporan';
        
        $data_param['jenjang']=$this->session->userdata('handling')->jenjang;
        
        switch($this->session->userdata('handling')->instansi){
            case 'PUSAT':
                
            break;
            case 'PROPINSI':    
                $data_param['id_prov']=$this->session->userdata('handling')->propinsi;
                
            break;
            case 'DAERAH':
                $data_param['id_kabkota']=$this->session->userdata('handling')->kabupaten;
            break;
        }
        
        if($tahun!='all')
            $data_param['tahun']=$tahun;
        
        if($triwulan!='all')
            $data_param['triwulan']=$triwulan;
            
        $data['report']=$this->Select_db->report_kategori('by_status',$data_param);
        
        $this->load->view('handling/main_view',$data);
    }
    
    function r5($tahun=null, $triwulan=null){
        $this->initlib->cek_session_handling();
        
        $data['title'] = 'Aplikasi P3M : Laporan';
        $data['module']='mod_laporan';
        
        $data_param['jenjang']=$this->session->userdata('handling')->jenjang;
        
        switch($this->session->userdata('handling')->instansi){
            case 'PUSAT':
                
            break;
            case 'PROPINSI':    
                $data_param['id_prov']=$this->session->userdata('handling')->propinsi;
                
            break;
            case 'DAERAH':
                $data_param['id_kabkota']=$this->session->userdata('handling')->kabupaten;
            break;
        }
        
        if($tahun!='all')
            $data_param['tahun']=$tahun;
        
        if($triwulan!='all')
            $data_param['triwulan']=$triwulan;
            
        $data['report']=$this->Select_db->report_pelaku(null,$data_param);
        
        $this->load->view('handling/main_view',$data);
    }
    
    function r6($tahun=null, $triwulan=null){
        $this->initlib->cek_session_handling();
        
        $data['title'] = 'Aplikasi P3M : Laporan';
        $data['module']='mod_laporan';
        $data['daerah']='all';
        
        $data_param['jenjang']=$this->session->userdata('handling')->jenjang;
        
        switch($this->session->userdata('handling')->instansi){
            case 'PUSAT':
                
            break;
            case 'PROPINSI':    
                $data_param['id_prov']=$this->session->userdata('handling')->propinsi;
                $data['daerah']='provinsi';
            break;
            case 'DAERAH':
                $data_param['id_kabkota']=$this->session->userdata('handling')->kabupaten;
                $data['daerah']='kabkota';
            break;
        }
        
        if($tahun!='all')
            $data_param['tahun']=$tahun;
        
        if($triwulan!='all')
            $data_param['triwulan']=$triwulan;
            
        $data['report']=$this->Select_db->report_daerah(null,$data_param);
        
        $this->load->view('handling/main_view',$data);
    }
    
    function r7($tahun=null, $triwulan=null){
        $this->initlib->cek_session_handling();
        
        $data['title'] = 'Aplikasi P3M : Laporan';
        $data['module']='mod_laporan';
        
        $data_param['jenjang']=$this->session->userdata('handling')->jenjang;
        
        switch($this->session->userdata('handling')->instansi){
            case 'PUSAT':
                
            break;
            case 'PROPINSI':    
                $data_param['id_prov']=$this->session->userdata('handling')->propinsi;
                
            break;
            case 'DAERAH':
                $data_param['id_kabkota']=$this->session->userdata('handling')->kabupaten;
            break;
        }
        
        if($tahun!='all')
            $data_param['tahun']=$tahun;
        
        if($triwulan!='all')
            $data_param['triwulan']=$triwulan;
            
        $data['report']=$this->Select_db->report_media(null,$data_param);
        
        $this->load->view('handling/main_view',$data);
    }
}