<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Forgot_password extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->helper(array('url','date','recaptchalib'));
        $this->load->model(array('Select_db','Insert_db','Update_db'));
        $this->load->library(array('initlib','session','email'));
        $this->load->database();
    }
    
    function index(){
        $data['title']='Ganti Password';
        $data['publickey']='6LcJrc8SAAAAAEpxDuxrndytsgi3C9zpF_tYBATn';
        $this->load->view('home/forgot_password_view',$data);
    }
    
    function reset_password($hash=''){
        if($this->session->userdata('hash_token_reset') && $hash==''){
            //reset password
            $data['title']='Ganti Password';
            
            if($this->session->flashdata('success')){
                $this->output->set_header('refresh:5;url='.site_url('')); 
                $this->session->unset_userdata('hash_token_reset');
            }
            $this->load->view('home/reset_password_view',$data);
            
        }else{
            //redirect hash simpen di session
            $query=$this->Select_db->t_user('by_hash_token_reset', array('hash_token_reset' => $hash));
            if($query->num_rows()==1){
                $user=$query->row();
                $this->session->set_userdata('hash_token_reset',$hash);
                redirect('forgot_password/reset_password');
            }else{
                redirect('');
            }
        }
        
    }
    
    function post_reset_password(){
        if($this->session->userdata('hash_token_reset') && $this->input->post('password') == $this->input->post('password1')){
            $query=$this->Select_db->t_user('by_hash_token_reset', array('hash_token_reset' => $this->session->userdata('hash_token_reset')));
            if($query->num_rows()==1){
                $user=$query->row();
                
                //change password
                if($this->Update_db->t_user($user->id, array('password' => md5($user->userid.':'.$this->input->post('password')), 'hash_token_reset' => ''))){
                    //send email notifikasi
                    $data=array(
                        'nama' => $user->username,
                        'link_login' => site_url(''),
                        'username' => $user->userid,
                        'password' => $this->input->post('password')
                    );
                    $body=$this->load->view('home/send_email_forgot_password_success_view',$data,true);
                    $this->Insert_db->send_email(array(
                        'from' => 'bos@kemdikbud.go.id|BOS kemdikbud',
                        'to' => $user->email,
                        'subject' => 'notifikasi pergantian password P3M BOS',
                        'body' => $body	
                    ));
                    $this->session->set_flashdata('success','Password berhasil diganti.');    
                }else{
                    $this->session->set_flashdata('error','Password gagal diganti.');
                }
                
            }else{
                $this->session->set_flashdata('error', 'duplikat token!.');
            }
        }else{
            $this->session->set_flashdata('error','Password tidak sama, silahkan diulangi kembali.');
        }
        redirect('forgot_password/reset_password');
    }
    
    function post_forgot_password(){
        $privatekey='6LcJrc8SAAAAAOhFYtWJWq3A4ZuK_3gjV22WqOq0';
        $resp = recaptcha_check_answer (
          $privatekey,
          $_SERVER["REMOTE_ADDR"],
          $_POST["recaptcha_challenge_field"],
          $_POST["recaptcha_response_field"]
        );
        
        if ($resp->is_valid) {
            $query=$this->Select_db->t_user('by_email',array('email'=>$this->input->post('email')));
            //echo $query->last_result(); die();
            if($query->num_rows()==1){
                //sent email reset
                $user=$query->row();
                $email=explode(',',$user->email);
                
                $check_email=0;
                foreach($email as $row_email){
                    if(strtolower($this->input->post('email')) == strtolower($row_email)){
                        //valid kirim email
                        
                        //generate token
                        $token=md5(time().$this->input->post('email').time());
                        
                        $data['link_reset']=site_url('forgot_password/reset_password/'.$token);
                        $data['nama']=$user->username;
                        $body=$this->load->view('home/send_email_forgot_password_view',$data,true);
                        if($this->Update_db->t_user($user->id, array('hash_token_reset' => $token))){
                            $check_email=1;
                            $this->Insert_db->send_email(array(
                                'from' => 'bos@kemdikbud.go.id|BOS kemdikbud',
                                'to' => $row_email,
                                'subject' => 'reset password P3M BOS',
                                'body' => $body	
                            ));
                        }else{
                            $this->session->set_flashdata('error','update fail!');    
                        }
                    }
                }
                
                //status check email
                if($check_email){
                    $this->session->set_flashdata('success','silahkan cek kotak masuk email anda.');
                }else{
                    $this->session->set_flashdata('error','gagal kirim email!.');
                }
            }else{
                $this->session->set_flashdata('error','data tidak ditemukan!.');
            }
        }else{
        $this->session->set_flashdata('error_captcha','CAPTCHA yang dimasukan salah!.');
    }
    redirect('forgot_password');
    }
}
       

?>