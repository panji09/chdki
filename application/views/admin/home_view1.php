<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php require_once("inc/head_admin.php") ?>
<body>
<!-- Start: page-top-outer -->
<div id="page-top-outer">    

<!-- Start: page-top -->
<div id="page-top">

	<!-- start logo -->
	<?php require_once('inc/logo.php'); ?>
	<!-- end logo -->
	
	<!--  start top-search -->
	<div id="top-search">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><!--<input type="text" value="Search" onblur="if (this.value=='') { this.value='Search'; }" onfocus="if (this.value=='Search') { this.value=''; }" class="top-search-inp" />--></td>
		<td>
		<!--<select  class="styledselect">
			<option value=""> All</option>
			<option value=""> Products</option>
			<option value=""> Categories</option>
			<option value="">Clients</option>
			<option value="">News</option>
		</select>--> 
		</td>
		<td>
		<!--<input type="image" src="images/shared/top_search_btn.gif"  />-->
		</td>
		</tr>
		</table>
	</div>
 	<!--  end top-search -->
 	<div class="clear"></div>

</div>
<!-- End: page-top -->

</div>
<!-- End: page-top-outer -->
	
<div class="clear">&nbsp;</div>
 
<!--  start nav-outer-repeat................................................................................................. START -->
<?php require_once('inc/menu_admin.php'); ?>
<div class="clear"></div>
<!--  start nav-outer -->
</div>
<!--  start nav-outer-repeat................................................... END -->

 <div class="clear"></div>
 
<!-- start content-outer ........................................................................................................................START -->
<div id="content-outer">
<!-- start content -->
<div id="content">

	<!--  start page-heading -->
	<div id="page-heading">
		<h1>Pengaduan Online</h1>
	</div>
	<!-- end page-heading -->

	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
	<tr>
		<th rowspan="3" class="sized"><img src="<?=$this->config->item('admin_img')?>/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
		<th class="topleft"></th>
		<td id="tbl-border-top">&nbsp;</td>
		<th class="topright"></th>
		<th rowspan="3" class="sized"><img src="<?=$this->config->item('admin_img')?>/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
	</tr>
	<tr>
		<td id="tbl-border-left"></td>
		<td>
		<!--  start content-table-inner ...................................................................... START -->
		<div id="content-table-inner">
		
			<!--  start table-content  -->
			<div id="table-content">
			
				<!-- isi msg -->
		
		 
				<!--  start product-table ..................................................................................... -->
				<form id="mainform" action="<?=site_url('administrator/post_table')?>" method="post">
				<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table">
				<tr>
					<th class="table-header-check"><a id="toggle-all" ></a> </th>
					<th class="table-header-repeat"><a href="">No </a></th>
					<th class="table-header-repeat line-left"><a href="">Status </a></th>
					<th class="table-header-repeat line-left minwidth-1"><a href="">Tanggal</a>	</th>
					<th class="table-header-repeat line-left minwidth-1"><a href="">Kategori </a></th>
					<th class="table-header-repeat line-left minwidth-1"><a href="">Kode Pengaduan </a></th>
					<th class="table-header-repeat line-left"><a href="">Deskripsi</a></th>
					<th class="table-header-repeat line-left"><a href="">Sekolah</a></th>
					<th class="table-header-repeat line-left"><a href="">Lokasi</a></th>
					<th class="table-header-options line-left"><a href="">Action</a></th>
				</tr>
				<?php
					
					$no=($this->uri->segment(3)=='' ? 0 : $this->uri->segment(3));
					
				?>
				<?php foreach($ponline->result() as $row):?>
				<tr>
					<td>
						<?php if($row->status !="terima"):?>
							<input value="<?=$row->id?>" type="checkbox" name="rowid[]"/>
						<?php endif;?>					</td>
					<td><?=++$no?></td>
					<td><?=$row->status?></td>
					<td><?=mysqldatetime_to_date($row->tanggal, $format = "d/m/Y, H:i:s")?></td>
					<td><?=$row->kategori?></td>
					<td><?=$row->kode_pengaduan?></td>
					<td><?=$row->deskripsi?></td>
					<td><?=$row->sekolah?></td>
					<td><?=$row->lokasi?></td>
					<td class="options-width">
					<a  class="iframe" href="<?=site_url('administrator/index/view/'.$row->id)?>" >Lihat</a>
					<?php if($row->status !="terima"):?>
					 | <a href="<?=site_url('administrator/post_table/terima/'.$row->id)?>" >Terima</a> |
					<a href="<?=site_url('administrator/post_table/delete/'.$row->id)?>" >Tolak</a> 
					<?php endif;?>
					
					
					
<!--					<a href="" title="Edit" class="icon-3 info-tooltip"></a>
					<a href="" title="Edit" class="icon-4 info-tooltip"></a>
					<a href="" title="Edit" class="icon-5 info-tooltip"></a>					--></td>
				</tr>
				<?php endforeach;?>
				</table>
				<!--  end product-table................................... --> 
				
			</div>
			<!--  end content-table  -->
		
			<!--  start actions-box ............................................... -->
			<div id="actions-box">
				<select id="list_action" name="list_action">
					<option value="">-pilih action-</option>
					<option value="terima">Terima</option>
					<option value="delete">Tolak</option>
				</select>
				<!--<a href="" class="action-slider"></a>
				<div id="actions-box-slider">
					<a href="" class="action-edit" onclick="$('#mainform').submit()">Verify</a>
					<a href="" class="action-delete">Register</a>
				</div>-->
				<div class="clear"></div>
			</div>
			</form>
			<!-- end actions-box........... -->
			
			<!--  start paging..................................................... -->
			<table border="0" cellpadding="0" cellspacing="0" id="paging-table">
			<tr>
			<td> &nbsp; Total <?=$num_rows?> &nbsp;
				<!--<a href="" class="page-far-left"></a>
				<a href="" class="page-left"></a>
				<div id="page-info">Page <strong>1</strong> / 15</div>
				<a href="" class="page-right"></a>
				<a href="" class="page-far-right"></a>-->
			</td>
			<td> &nbsp;
				<?=$this->pagination_admin->create_links();?>
				&nbsp; 
			</td>
			<td>
			<?php
				$num_rows=array(10,30,50);
			?>
			<!--<select  class="styledselect_pages">-->
			<form id="form_num_rows" method="post" action="<?=site_url('administrator/post_num_rows')?>">
			<input type="hidden" name="url" value="<?=current_url()?>"/>
			<select id="num_rows" name="num_rows">
				<option value="">Number of rows</option>
				<?php foreach($num_rows as $row):?>
					<?php if($row == $this->session->userdata('num_rows')):?>
						<option value="<?=$row?>" selected="selected"><?=$row?></option>
					<?php else:?>
						<option value="<?=$row?>"><?=$row?></option>
					<?php endif;?>
				<?php endforeach;?>
			</select>
			</form>
			</td>
			</tr>
			</table>
			
			
			<!--  end paging................ -->
			
			<div class="clear"></div>
		 
		</div>
		<!--  end content-table-inner ............................................END  -->
		</td>
		<td id="tbl-border-right"></td>
	</tr>
	<tr>
		<th class="sized bottomleft"></th>
		<td id="tbl-border-bottom">&nbsp;</td>
		<th class="sized bottomright"></th>
	</tr>
	</table>
	<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer........................................................END -->

<div class="clear">&nbsp;</div>
    
<!-- start footer -->         
<?php require_once('inc/footer.php') ?>
<!-- end footer -->
 
</body>
</html>