<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php require_once("inc/head_admin.php") ?>
<body>
<!-- Start: page-top-outer -->
<div id="page-top-outer">    

<!-- Start: page-top -->
<div id="page-top">

	<!-- start logo -->
	<?php require_once('inc/logo.php'); ?>
	<!-- end logo -->
	
	<!--  start top-search -->
	<div id="top-search">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
		<td><!--<input type="text" value="Search" onblur="if (this.value=='') { this.value='Search'; }" onfocus="if (this.value=='Search') { this.value=''; }" class="top-search-inp" />--></td>
		<td>
		<!--<select  class="styledselect">
			<option value=""> All</option>
			<option value=""> Products</option>
			<option value=""> Categories</option>
			<option value="">Clients</option>
			<option value="">News</option>
		</select>--> 
		</td>
		<td>
		<!--<input type="image" src="images/shared/top_search_btn.gif"  />-->
		</td>
		</tr>
		</table>
	</div>
 	<!--  end top-search -->
 	<div class="clear"></div>

</div>
<!-- End: page-top -->

</div>
<!-- End: page-top-outer -->
	
<div class="clear">&nbsp;</div>
 
<!--  start nav-outer-repeat................................................................................................. START -->
<?php require_once('inc/menu_admin.php'); ?>
<div class="clear"></div>
<!--  start nav-outer -->
</div>
<!--  start nav-outer-repeat................................................... END -->

 <div class="clear"></div>
 
<!-- start content-outer ........................................................................................................................START -->
<div id="content-outer">
<!-- start content -->
<div id="content">

	<!--  start page-heading -->
	<div id="page-heading">
		<h1>Berita </h1>
	</div>
	<!-- end page-heading -->

	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
	<tr>
		<th rowspan="3" class="sized"><img src="<?=$this->config->item('admin_img')?>/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
		<th class="topleft"></th>
		<td id="tbl-border-top">&nbsp;</td>
		<th class="topright"></th>
		<th rowspan="3" class="sized"><img src="<?=$this->config->item('admin_img')?>/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
	</tr>
	<tr>
		<td id="tbl-border-left"></td>
		<td>
		<!--  start content-table-inner ...................................................................... START -->
		<div id="content-table-inner">
		
			<!--  start table-content  -->
			<div id="table-content">
			<?php if($this->uri->segment(3)=="view"):?>
			<table cellspacing="0" cellpadding="0" border="0" id="id-form">
              <tr>
                <th valign="top">Tanggal</th>
                <td><?=mysqldatetime_to_date($berita->tanggal, $format = "d/m/Y, H:i:s")?></td>
                <td></td>
              </tr>
              <tbody>
                <tr>
                  <th valign="top">Judul:</th>
                  <td><?=$berita->judul;?></td>
                  <td></td>
                </tr>
                <tr>
                  <th valign="top">Ringkasan:</th>
                  <td><?=$berita->ringkasan;?></td>
                  <td></td>
                </tr>
                <tr>
                  <th valign="top">Isi:</th>
                  <td><?=$berita->isi;?></td>
                  <td></td>
                </tr>
                <tr>
                  <th valign="top">&nbsp;</th>
                  <td><a href="<?=site_url('administrator/berita')?>" />Kembali</a></td>
                  <td></td>
                </tr>
              </tbody>
			  </table>
				<?php endif;?>
			</div>
			<!--  end table-content  -->
	
			<div class="clear"></div>
		 
		</div>
		<!--  end content-table-inner ............................................END  -->
		</td>
		<td id="tbl-border-right"></td>
	</tr>
	<tr>
		<th class="sized bottomleft"></th>
		<td id="tbl-border-bottom">&nbsp;</td>
		<th class="sized bottomright"></th>
	</tr>
	</table>
	<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer........................................................END -->

<div class="clear">&nbsp;</div>
    
<!-- start footer -->         
<?php require_once('inc/footer.php') ?>
<!-- end footer -->
 
</body>
</html>