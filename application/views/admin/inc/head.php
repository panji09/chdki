<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Pengaduan Online Admin</title>
<link rel="icon" type="image/png" href="<?=$this->config->item('home_img')?>/favicon.png" />
<link rel="stylesheet" href="<?=$this->config->item('admin_css')?>/screen.css" type="text/css" media="screen" title="default" />
<!--  jquery core -->
<script src="<?=$this->config->item('admin_js')?>/jquery/jquery-1.4.1.min.js" type="text/javascript"></script>

<!-- Custom jquery scripts -->
<script src="<?=$this->config->item('admin_js')?>/jquery/custom_jquery.js" type="text/javascript"></script>

<!-- MUST BE THE LAST SCRIPT IN <HEAD></HEAD></HEAD> png fix -->
<script src="<?=$this->config->item('admin_js')?>/jquery/jquery.pngFix.pack.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
$(document).pngFix( );
});
</script>
</head>