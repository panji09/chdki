<!-- Modal logout-->
<div class="modal fade" id="modal_logout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" ><span class="glyphicon glyphicon-question-sign"></span> Konfirmasi</h4>
      </div>
      <div class="modal-body">
        Apakah anda ingin keluar?
      </div><!--end modal body-->
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <a href='<?=site_url('handling/login/logout')?>' class="btn btn-primary">Logout</a>
      </div>
    </div>
    
  </div>
</div>
<!-- end Modal logout-->

<!-- modal user info, ganti password, bantuan-->
<div class="modal fade" id="modal_form_layout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" >##</h4>
      </div>
      <form  role="form" method='post' action='#'>
      <div class="modal-body">
        
      </div><!--end modal body-->
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>
    
  </div>
</div>
<!-- end modal user info, ganti password, bantuan-->

<!--ganti password-->
<div class="modal fade" id="modal_ganti_password" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" >Ganti Password</h4>
      </div>
      <form  role="form" method='post' action='<?=site_url('handling/admin/post_ganti_password')?>'>
      <div class="modal-body">
        <div class="form-group">
	    <label for="exampleInputEmail1">Password Lama</label>
	    <input type="password" class="form-control" placeholder="Password Lama" name='password_lama'>
	</div>
	<div class="form-group">
	    <label for="exampleInputPassword1">Password Baru</label>
	    <input type="password" class="form-control" placeholder="password Baru" name='password_baru'>
	</div>
      </div><!--end modal body-->
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>
    
  </div>
</div>
<!--./ganti password-->
<!--modal bantuan-->
<div class="modal fade" id="modal_bantuan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" >Bantuan</h4>
      </div>
      
      <div class="modal-body">
        <strong>Panji A. Musthofa</strong><br>
        Email : panji09@gmail.com<br>
        SMS/Telp : 081220256161
      </div><!--end modal body-->
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    
  </div>
</div>
<!--./modal bantuan-->


<!--modal pengaduan-->
<?php if($this->uri->segment(1)=='handling' || $this->uri->segment(2)=='handling'):?>
<div class="modal fade" id="modal_pengaduan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="modal_pengaduan_label">Input Pengaduan</h4>
      </div>
      <form  role="form" method='post' action=''>
      <div class="modal-body">
	  <?=$this->load->view('handling/modal_pengaduan_view');?>
      </div><!--end modal body-->
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
    </form>
    </div>
    
  </div>
</div>
<!-- End Modal pengaduan -->

<!-- modal jawab-->
<div class="modal fade" id='modal_jawab' tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Respon Pengaduan</h4>
      </div>
      <form  role="form" method='post' action=''>
      <div class="modal-body">
        
      </div><!--end modal-body-->
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!--end modal jawab-->

<!-- modal hapus-->
<div class="modal fade" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" ></h4>
      </div>
      <form  role="form" method='post' action='#'>
      <div class="modal-body">
        
      </div><!--end modal body-->
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <a id='modal_delete_href' href='' class="btn btn-primary">Hapus</a>
      </div>
      </form>
    </div>
    
  </div>
</div>
<!-- end modal hapus-->
<?php endif;?>

<!-- verifikasi pengaduan -->
    <div class="modal fade" id="modal_approved" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    <h4 class="modal-title" id="myModalLabel">Terima</h4>
	  </div>
	  <form method='post' action='' role="form" class="form-horizontal">
	  <div class="modal-body">
	      
	      
	  </div>
	  <div class="modal-footer">
	    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
	    <input type="submit" class="btn btn-primary" value='Terima'>
	  </div>
	  </form>
	</div>
      </div>
    </div>
    <!-- ./verifikasi reason -->
    
    <!-- delete reason -->
    <div class="modal fade" id="modal_delete_reason" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    <h4 class="modal-title" id="myModalLabel">Konfirmasi</h4>
	  </div>
	  <form method='post' action='' role="form">
	  <div class="modal-body">
	      <div class="form-group">
		  <label for="">Alasan?</label>
		  <textarea class="form-control" name='reason_deleted'></textarea>
	      </div>
	  </div>
	  <div class="modal-footer">
	    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
	    <input type="submit" class="btn btn-primary" value='Hapus'>
	  </div>
	  </form>
	</div>
      </div>
    </div>
    <!-- ./delete reason -->