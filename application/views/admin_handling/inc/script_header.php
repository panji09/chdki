<!-- Bootstrap -->
    <link rel="stylesheet" href="<?=$this->config->item('plugin')?>/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=$this->config->item('handling_plugin')?>/datatables-bootstrap/css/datatables.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?=$this->config->item('plugin')?>/Font-Awesome/css/font-awesome.min.css">

    <!-- Metis core stylesheet -->
    <link rel="stylesheet" href="<?=$this->config->item('admin_handling_css')?>/main.css">
    <link rel="stylesheet" href="<?=$this->config->item('admin_handling_css')?>/theme.css">
    <link rel="stylesheet" href="<?=$this->config->item('plugin')?>/fullcalendar-1.6.2/fullcalendar/fullcalendar.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>
      <script src="<?=$this->config->item('plugin')?>/html5shiv/html5shiv.js"></script>
	      <script src="<?=$this->config->item('plugin')?>/respond/respond.min.js"></script>
	    <![endif]-->

    
    
    <link rel="stylesheet" href="<?=$this->config->item('plugin')?>/jquery-file-upload/css/blueimp-gallery.min.css">
    <link rel="stylesheet" href="<?=$this->config->item('plugin')?>/jquery-file-upload/css/jquery.fileupload.css">
    <link rel="stylesheet" href="<?=$this->config->item('plugin')?>/jquery-file-upload/css/jquery.fileupload-ui.css">
    <!-- CSS adjustments for browsers with JavaScript disabled -->
    <noscript><link rel="stylesheet" href="<?=$this->config->item('plugin')?>/jquery-file-upload/css/jquery.fileupload-noscript.css"></noscript>
    <noscript><link rel="stylesheet" href="<?=$this->config->item('plugin')?>/jquery-file-upload/css/jquery.fileupload-ui-noscript.css"></noscript>