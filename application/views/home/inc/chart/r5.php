<?php 
// kategori & status
if($report=true):?>
<style>
table.jqplot-table-legend, table.jqplot-cursor-legend {
	font-size: 1em;
}
.jqplot-axis {
	font-size: 1em;
}
.jqplot-point-label{
	font-size: 1em;
}
</style>
<div id="chart_pelaku" align="center" style="height:450px"><!--Chart Di Load disini--></div>

<script>
$(document).ready(function(){
	var proses = [0,1,11,0,0,0,1];
	var selesai = [9,24,381,0,122,8,183];
	var ticks = ['Komite Sekolah','Guru/Pegawai Sekolah','Kepala/Bendahara Sekolah','LSM','Dinas Pendidikan','UPTD','Lain-Lain'];
	 
	plot2 = $.jqplot('chart_pelaku', [proses, selesai], {
		animate: !$.jqplot.use_excanvas,
		seriesDefaults: {
			renderer:$.jqplot.BarRenderer,
                pointLabels: { show: true, location: 'e', edgeTolerance: -15 },
                shadowAngle: 135,
			rendererOptions: {
                    barDirection: 'horizontal'
                }
		},
		axes: {
			
			yaxis: {
				renderer: $.jqplot.CategoryAxisRenderer,
				ticks: ticks,
				
			},
			xaxis: {
				min:0,
				tickInterval: 20,
				tickOptions: { formatString:'%d' }
			},
		},
		legend: {
			show: true,
			location: 'ne',
			placement: 'inside'
		},
		series:[
			{label:'Proses'},
			{label:'Selesai'}
	   ],
	   title:{
			text:'JUMLAH STATUS PENGADUAN BERDASARKAN PELAKU <br>& STATUS PENANGANAN TAHUN 2011 s/d 2014'
	   }
	});
 
	
});
</script>

<p align="center"><strong>Keterangan : </strong>yang dimaksud dengan status pengaduan adalah progres penanganan pengaduan (proses / selesai)</p>

<table id="t_san" width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
	<th>Pelaku</th>
	<th>Proses</th>
	<th>Selesai</th>
	<th>Total</th>
</tr>
<tr>
	<td>Komite Sekolah</td>
	<td>0</td>
	<td>9</td>
	<td>9</td>
</tr>
<tr>
	<td>Guru/Pegawai Sekolah</td>
	<td>1</td>
	<td>24</td>
	<td>25</td>
</tr>
<tr>
	<td>Kepala/Bendahara Sekolah</td>
	<td>11</td>
	<td>381</td>
	<td>392</td>
</tr>
<tr>
	<td>LSM</td>
	<td>0</td>
	<td>0</td>
	<td>0</td>
</tr>
<tr>
	<td>Dinas Pendidikan</td>
	<td>0</td>
	<td>122</td>
	<td>122</td>
</tr>

<tr>
	<td>UPTD</td>
	<td>0</td>
	<td>8</td>
	<td>8</td>
</tr>
<tr>
	<td>Lain-Lain</td>
	<td>1</td>
	<td>183</td>
	<td>184</td>
</tr>
</table>
<?php else:?>
<div id="chart_kategori" align="center">Data Kosong</div>
<?php endif;?>