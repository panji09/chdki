<?php 
// kategori & status
if($report=true):?>
<style>
table.jqplot-table-legend, table.jqplot-cursor-legend {
	font-size: 1em;
}
.jqplot-axis {
	font-size: 1em;
}
.jqplot-point-label{
	font-size: 1em;
}
</style>
<div id="chart_kategori" align="center" style="height:450px"><!--Chart Di Load disini--></div>

<script>
$(document).ready(function(){
	var data = [['Pertanyaan',1800],['Saran',248],['Pengaduan',601]];
	 
	plot2 = $.jqplot('chart_kategori', [data], {
		seriesDefaults:{
            renderer:$.jqplot.PieRenderer,
            trendline:{ show:false },
            rendererOptions: { padding: 8, sliceMargin: 4, showDataLabels: true }
        },
        legend:{
            show:true,
            placement: 'inside',
            location:'ne'
        },
		
	   title:{
			text:'JUMLAH PENGADUAN BERDASARKAN KATEGORI<br>TAHUN 2014'
	   }
	});
 
	
});
</script>

<p align="center"><strong>Keterangan : </strong>yang dimaksud dengan status pengaduan adalah progres penanganan pengaduan (proses / selesai)</p>


<table id="t_san" width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
	<th>Kategori</th>
	<th>Total</th>
</tr>
<tr>
	<td>Pertanyaan</td>
	<td>1800</td>
	</tr>
<tr>
	<td>Saran</td>
	<td>248</td>
	</tr>
<tr>
	<td>Pengaduan</td>
	<td>601</td>
	</tr>

	<td><strong>Total</strong></td>
	<td><strong>2790</strong></td>
</tr>
</table>

<?php else:?>
<div id="chart_kategori" align="center">Data Kosong</div>
<?php endif;?>