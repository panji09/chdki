<header>
  	<div class="main">
      <div><h1><a href="<?=site_url('')?>"><img src="<?=$this->config->item('home_img')?>/logo_BOP1.png" alt=""></a></h1></div>
      <div style='position: relative;top: 39px;'><h3 style='color:#999999'>Pelayanan dan Penanganan Pengaduan Masyarakat</h3></div>
      <div style='position: absolute; top: 9px; left:800px'><h1><a href="<?=site_url('')?>"><img src="<?=$this->config->item('home_img')?>/logo_kjp_mini.png" alt=""></a></h1></div>
      <nav>
          <ul class="menu">
              <li <?=($this->uri->segment(1)=='' || $this->uri->segment(2)=='berita' || $this->uri->segment(2)=='artikel' ? 'class="current"' : '')?>><a href="<?=site_url('')?>">Home</a></li>
              <li <?=($this->uri->segment(2)=='about' ? 'class="current"' : '')?>><a href="<?=site_url('home/about')?>">Tentang</a>
		  <ul class="menu">
		      <li><a href="<?=site_url('home/about/kjp')?>">KJP (kartu Jakarta Pintar)</a></li>
		      <li><a href="<?=site_url('home/about/bop')?>">BOP (Biaya Operasional Pendidikan)</a></li>
		  </ul>
              </li>
              <li <?=($this->uri->segment(2)=='graph' ? 'class="current"' : '')?>><a href="<?=site_url('home/graph')?>">Statistik Pengaduan</a></li>
			  <li <?=($this->uri->segment(2)=='pengaduan' ? 'class="current"' : '')?>><a href="<?=site_url('home/pengaduan')?>">Pengaduan Online</a></li>
              <li <?=($this->uri->segment(2)=='lihat_pengaduan' || $this->uri->segment(2)=='lihat_pengaduan_sms' ? 'class="current"' : '')?>><a href="<?=site_url('home/lihat_pengaduan')?>">Lihat Pengaduan</a>
			  	<!--
				<ul>
					<li><a href="<?=site_url('home/lihat_pengaduan')?>">Online</a></li>
					<li><a href="<?=site_url('home/lihat_pengaduan_sms')?>">SMS</a></li>
				</ul>
				-->
		</li>
              <li <?=($this->uri->segment(2)=='faq' ? 'class="current"' : '')?>><a href="<?=site_url('home/faq')?>">FAQ</a></li>
          </ul>    
      </nav>
    </div> 
</header> 