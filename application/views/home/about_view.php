<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<?php include('inc/head.php');?>
<body>
<!--==============================header=================================-->
<?php include('inc/menu.php');?>
<!--==============================content================================-->
<section id="content" class="border"><div class="ic"></div>
	<div class="container_12">
      
	  <!-- content -->
      <div class="grid_8">
      	<div class="page1-col1">
			
			<div class="judul_page">
				<div class="breadcrumb" style="font-size:13px">
					<ul id="breadcrumbs-four">
						<li><a href="<?=site_url('')?>">Home</a></li>
						<li><a href="#" class="current">Tentang <?=strtoupper($this->uri->segment(3))?></a></li>
					</ul>
				</div>
				<img src="<?=$this->config->item('home_img')?>/icon_news.png" /> Tentang <?=strtoupper($this->uri->segment(3))?>
			</div>
	
		
		<?php if($this->uri->segment(3)==''):?>
		<p>
		<ul style='list-style-type: square;'>
		    <li><a href='<?=site_url('home/about/bop')?>'>Tentang BOP</a></li>
		    <li><a href='<?=site_url('home/about/kjp')?>'>Tentang KJP</a></li>
		</ul>
		</p>
		<?php else:?>
		    <?=$tentang->content?>
		<?php endif;?>
		<?php include('inc/share_button.php');?>
	
	
			

            
            
        </div>
      </div>
	  <!-- content end -->
	  
	  <!-- sidebar -->
      <?php include('inc/menu_kanan.php');?>
	  <!-- sidebar end -->
      <div class="clear"></div>
    </div>
</section> 
<!--==============================footer=================================-->
<?php include('inc/footer.php');?>		
<script>
	Cufon.now();
</script>
</body>
</html>