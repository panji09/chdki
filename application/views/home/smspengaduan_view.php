<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Layanan Masyarakat dana BOS</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="icon" type="image/png" href="<?=$this->config->item('home_img')?>/favicon.png" />
<style>
@charset "utf-8";
body {
	margin:0;
	padding:0;
	width:100%;
	color:#959595;
	font:normal 12px/1.8em Arial, Helvetica, sans-serif;
	background:/*#f5f5f5*/ url(../images/footer_bg.gif) repeat-x center bottom;
}
html, .main {
	padding:0;
	margin:0;
}
.main {
	/*background:#025F99;
	background:url(../images/bg.gif) repeat-x center top;*/
}
.clr {
	clear:both;
	padding:0;
	margin:0;
	width:100%;
	font-size:0;
	line-height:0;
}
.logo {
	padding:0;
	float:left;
	width:auto;
}
h1 {
	margin:0;
	padding:29px 24px;
	float:left;
	color:#e4e5e5;
	font:bold 36px/1.2em Arial, Helvetica, sans-serif;
	letter-spacing:-2px;
}
h1 a, h1 a:hover {
	color:#e4e5e5;
	text-decoration:none;
}
h1 span {
	color:#02c7fe;
}
h1 small {
	font:normal 12px/1.2em Arial, Helvetica, sans-serif;
	letter-spacing:normal;
	padding-left:8px;
	color:#e0e1e1;
}
h2 {
	font:normal 24px/1.5em Arial, Helvetica, sans-serif;
	padding:8px 0;
	margin:8px 0;
	color:#595959;
}
p {
	margin:8px 0;
	padding:0 0 8px 0;
	font:normal 12px/1.8em Arial, Helvetica, sans-serif;
}
a {
	color:#3B749C;
	text-decoration:underline;
}
p.spec {
}
p.infopost {
	line-height:30px;
}
a.rm {
}
a.com {
}
.header, .content, .menu_nav, .fbg, .footer, form, ol, ol li, ul, .content .mainbar, .content .sidebar {
	margin:0;
	padding:0;
}
.header {
}
.header_resize {
	margin:0 auto;
	padding:0;
	width:970px;
}
img.hbg_img {
	margin:0 0 8px;
	padding:11px;
	background-color:#fff;
	border-bottom:2px solid #e7e7e7;
}
.menu_nav {
	margin:0 auto;
	padding:0;
	width:1000px;
	height:48px;
	background:url(../images/menu.gif) repeat-x center top;
}
.menu_nav ul {
	list-style:none;
	height:48px;
	float:left;
}
.menu_nav ul ul{
	visibility:hidden;
	list-style:none;
		display:block;
	margin:0;
	padding:13px 24px;
	color:#888;
	text-decoration:none;
	font-size:13px;
}
.menu_nav ul li:hover ul,
.menu_nav ul a:hover ul{
	visibility:visible;
}
.menu_nav ul li {
	margin:0;
	padding:0 2px 0 0;
	float:left;
}
.menu_nav ul li a {
	display:block;
	margin:0;
	padding:13px 24px;
	color:#888;
	text-decoration:none;
	font-size:13px;
}

.menu_nav ul li.active a, .menu_nav ul li a:hover {
	color:#fff;
	background-color:#025F99;
}
.content {
}
.content_resize {
	margin:0 auto;
	padding:0 0 24px;
	width:970px;
	background-color:#fff;
	border-bottom:2px solid #e7e7e7;
	border-top:none;
}
.content .mainbar {
	margin:0;
	padding:0;
	float:left;
	width:570px;
}
.content .mainbar1 {
	margin:0;
	padding:0;
	float:left;
	width:784px;
}
.content .mainbar img {
	border:1px solid #d8dbdc;
	padding:4px;
}
.content .mainbar img.fl {
	margin:4px 16px 4px 0;
	float:left;
}
.content .mainbar .article {
	margin:0 0 12px;
	padding:0px 24px 0 24px;
}
div.article ul,
div.article ol {
	/*margin: 0 0 10px 20px;*/
}
.content .sidebar {
	padding:0;
	float:right;
	width:200px;
}
.sidebar1 {
	padding-left:10px;
	float:left;
	width:170px;
}
.content .sidebar .gadget .sidebar1 {
	margin:0 0 12px;
	padding:8px 16px 8px 24px;
}
ul.sb_menu, ul.ex_menu {
	margin:0;
	padding:0 0 0 16px;
	list-style:none;
	color:#959595;
}
ul.sb_menu li, ul.ex_menu li {
	margin:0;
}
ul.sb_menu li {
	padding:4px 0;
	width:220px;
}
ul.ex_menu li {
	padding:4px 0;
}
ul.sb_menu li a, ul.ex_menu li a {
	color:#959595;
	text-decoration:none;
	margin-left:-16px;
	padding:4px 8px 4px 16px;
	background:url(../images/li.gif) no-repeat left center;
}
ul.sb_menu li a:hover, ul.ex_menu li a:hover {
	color:#3B749C;
	font-weight:bold;
	text-decoration:none;
}
.content p.pages {
	padding:32px 24px 12px;
	font-size:11px;
	color:#959595;
	text-align:right;
}
.content p.pages span, .content p.pages a:hover {
	padding:5px 10px;
	color:#fff;
	background-color:#3B749C;
	border:1px solid #3B749C;
}
.content p.pages a {
	padding:5px 10px;
	color:#959595;
	background-color:#fff;
	border:1px solid #edebeb;
	text-decoration:none;
}
.content p.pages small {
	font-size:11px;
	float:left;
}
.content .mainbar .comment {
	margin:0;
	padding:16px 0 0 0;
}
.content .mainbar .comment img.userpic {
	border:1px solid #dedede;
	margin:10px 10px 10px 10px;
	padding:0;
	float:left;
}
.fbg {
	padding-top:12px;
}
.fbg_resize {
	margin:0 auto;
	width:922px;
	padding:12px 24px;
	border:2px solid #e7e7e7;
	background-color:#fff;
}
.fbg .c2 a, .fbg .c3 a {
	margin:0;
	padding:2px 4px;
	text-decoration:underline;
}
.fbg h2 {
}
.fbg img {
	margin:0 16px 16px 16px;
	padding:3px;
	background-color:#fff;
	border:1px solid #99bece;
}
.fbg .col {
	margin:0;
	float:left;
}
.fbg .c1 {
	padding:0 16px 0 0;
	width:266px;
}
.fbg .c2 {
	padding:0 16px;
	width:300px;
}
.fbg .c3 {
	padding:0 0 0 16px;
	width:260px;
}
.footer {
}
.footer_resize {
	margin:0 auto;
	padding:12px 24px;
	width:922px;
}
.footer p {
	margin:0;
	padding:4px 0;
	line-height:normal;
}
.footer a {
	color:#959595;
	padding:inherit;
	text-decoration:underline;
}
.footer a:hover {
	text-decoration:none;
}
.footer .lf {
	float:left;
}
.footer .rf {
	float:right;
}
ol {
	list-style:none;
}
ol li {
	display:block;
	clear:both;
}
ol li label {
	display:block;
	margin:0;
	padding:16px 0 0 0;
}
ol li input.text {
	width:480px;
	border:1px solid #c0c0c0;
	margin:0;
	padding:5px 2px;
	height:16px;
	background-color:#fff;
}
ol li textarea {
	width:480px;
	border:1px solid #c0c0c0;
	margin:0;
	padding:2px;
	background-color:#fff;
}
ol li .send {
	margin:16px 0 0 0;
}
.searchform {
	float:right;
	padding:6px;
}
#formsearch {
	margin:0;
	height:36px;
	padding:0;
	width:232px;
}
#formsearch span {
	display:block;
	margin:0;
	padding:0;
	float:left;
	background:#fff url(../images/search.gif) no-repeat top left;
}
#formsearch input.editbox_search {
	margin:0;
	padding:11px 6px 10px;
	float:left;
	width:181px;
	border:none;
	background:none;
	font:normal 12px/1.5em Arial, Helvetica, sans-serif;
	color:#a8acb2;
}
#formsearch input.button_search {
	margin:0;
	padding:0;
	border:none;
	float:left;
}
form {
    /*
    background: none repeat scroll 0 0 #F8F8F8;
    border: 1px solid #F0F0F0;
    margin: 0;
    padding: 15px 25px 25px 20px;
    */
}
.button_submit{
	background: none repeat scroll 0 0 #254A86;
    border: 1px solid #163362;
    border-radius: 4px 4px 4px 4px;
    box-shadow: 1px 1px 0 #FFFFFF;
    color: #FFFFFF;
    font-size: 12px;
    font-weight: bold;
    line-height: 1;
    padding: 10px 22px;
    text-shadow: -1px -1px 0 rgba(0, 0, 0, 0.25); cursor: pointer;
}
</style>
<script type="text/javascript" src="<?=$this->config->item('home_js')?>/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="<?=$this->config->item('home_js')?>/jquery.fusioncharts.js"></script>
<script type="text/javascript" src="<?=$this->config->item("home_js")?>/cufon-yui.js"></script>
<script type="text/javascript" src="<?=$this->config->item("home_js")?>/arial.js"></script>
<script type="text/javascript" src="<?=$this->config->item("home_js")?>/cuf_run.js"></script>
<script type="text/javascript" src="<?=$this->config->item("home_plugin")?>/piecemaker/web/scripts/swfobject/swfobject.js"></script>
 


	
<script type="text/javascript">
	$(document).ready(function() {
		$("#provinsi").change(function() {
			$("#kabkota").empty();
			$("#kabkota").append('<option value="<?=$this->uri->segment(2)=='' || $this->uri->segment(2)=='graph' ? 'all' : ''?>"><?=$this->uri->segment(2)=='' || $this->uri->segment(2)=='graph' ? '-Semua-' : '-Pilih-'?></option>');
			$("#kecamatan").empty();
			$("#kecamatan").append('<option value="">-Pilih-</option>');
			$("#desa").empty();
			$("#desa").append('<option value="">-Pilih-</option>');
			$.post('<?=site_url('services/get_kabkota')?>',
			{ KdProv : $("#provinsi option:selected").val() },
			function(data) {
				$.each(data, function(i, item){
					$("#kabkota").append(
						'<option value="' + item.KdKab + '">'+item.NmKab + '</option>'
					);
				})
			},
			'json'
			);
		});
		
		$("#kabkota").change(function() {
			$("#kecamatan").empty();
			$("#kecamatan").append('<option value="">-Pilih-</option>');
			$("#desa").empty();
			$("#desa").append('<option value="">-Pilih-</option>');
			$.post('<?=site_url('services/get_kecamatan')?>',
			{ KdKab : $("#kabkota option:selected").val() },
			function(data) {
				$.each(data, function(i, item){
					$("#kecamatan").append(
						'<option value="' + item.KdKec + '">'+item.NmKec + '</option>'
					);
				})
			},
			'json'
			);
		});
		
		$("#kecamatan").change(function() {
			$("#desa").empty();
			$("#desa").append('<option value="">-Pilih-</option>');
			$.post('<?=site_url('services/get_desa')?>',
			{ KdKec : $("#kecamatan option:selected").val() },
			function(data) {
				$.each(data, function(i, item){
					$("#desa").append(
						'<option value="' + item.KdDesa + '">'+item.NmDesa + '</option>'
					);
				})
			},
			'json'
			);
		});
		
		$("#jenjang").change(function() {
			$("#namasek").empty();
			if($("#jenjang").val()==4){
				$("#namasek").append('lokasi');
			}else{
				$("#namasek").append('Nama Sekolah');
			}
		});
		
		/*function showketRP(){
			if($('input[name=KdKategori]:checked').val()==4){
				$("#KetRpKat4").show();
			}else{
				$("#KetRpKat4").hide();
			}
		}
		showketRP();
		$('input[name=KdKategori]').click(function() {
			showketRP();
		});*/
		$("#KdKategori").change(function() {
			if($("#KdKategori").val()==4){
				$("#KetRpKat4_text").addClass("required");
				$("#KetRpKat4").show();
			}else{
				$("#KetRpKat4_text").attr('class', 'textInput auto validateInteger');
				$("#KetRpKat4").attr('class', 'ctrlHolder');
				$("#KetRpKat4").hide();
			}
		});
		function showlainSumber(){
			if($('input[name=KdSumber]:checked').val()==8){
				$("#LainSumber").show();
			}else{
				$("#LainSumber").hide();
			}
		}
		showlainSumber();
		$('input[name=KdSumber]').click(function() {
			showlainSumber();
		});
		
				
		function showpLain(){
			if($('input[name=pLain]:checked').val()){
				$("#KetpLain").show();
			}else{
				$("#KetpLain").hide();
			}
		}
		showpLain();
		$('input[name=pLain]').click(function() {
			showpLain();
		});
	});
	
</script>

	<style>
	.side-a {
		float: left;
		width: 30%;
	}
	
	.side-c {
		float: left;
		width: 30%;
	}
	
	.side-b { 
		float: left;
		width: 30%;
	}
	</style>
	<link href="<?=$this->config->item('home_css')?>/uni-form.css" media="all" rel="stylesheet"/>
	<link href="<?=$this->config->item('home_css')?>/default.uni-form.css" media="all" rel="stylesheet"/>
	
<!--jquery ui-->
<link rel="stylesheet" href="<?=$this->config->item("admin_js")?>/jquery-ui/themes/redmond/jquery.ui.all.css">
<script src="<?=$this->config->item("admin_js")?>/jquery-ui/ui/jquery-ui-1.8.18.custom.js"></script>
<!--end jquery ui-->
<!--tab-->
<script>
$(function() {
	$( "#tabs" ).tabs();
});
</script>
<!--end tab-->
<!--dialog-->
<script>
	// increase the default animation speed to exaggerate the effect
	$.fx.speeds._default = 1000;
	$(function() {
		$( "#dialog-registrasi" ).dialog({
			autoOpen: false,
			show: "blind",
			hide: "blind",
			
			width: '500px'
		});

		$( "#registrasi" ).click(function() {
			$( "#dialog-registrasi" ).dialog( "open" );
			return false;
		});
		$( "#dialog-forget" ).dialog({
			autoOpen: false,
			show: "blind",
			hide: "blind",
			
			width: '500px'
		});

		$( "#forget" ).click(function() {
			$( "#dialog-forget" ).dialog( "open" );
			return false;
		});
	});
	</script>
<!--end dialog-->
</head>
<body>
<div class="main">

          <div id="tabs">
    <ul>
        <li><a href="#tabs-1">Input Pengaduan</a></li>
        <li><a href="#tabs-2">SMS</a></li>
        
    </ul>
    <div id="tabs-1">
	  <style>
		  		#message-blue	{
				margin-bottom: 5px;
				}
			.blue-left	{
				background: url(<?=$this->config->item('admin_img')?>/table/message_blue.gif) top left no-repeat;
				color: #2e74b2;
				font-family: Tahoma;
				font-weight: bold;
				padding: 0 0 0 20px;
				}
			.blue-left a	{
				color: #2e74b2;
				font-family: Tahoma;
				font-weight: normal;
				text-decoration: underline;
				}
			.blue-right a	{
				cursor: pointer;
				}
			.blue-right	{
				width: 55px;}
		  </style>
		  <script>
			  $(document).ready(function() {
					$(".close-blue").click(function () {
						$("#message-blue").fadeOut("slow");
					});
					
				});
		  </script>
		  
			
			<form action="<?=site_url('administrator/post_pengaduan_sms')?>" method="post" class="uniForm">
			<input type="hidden" name="tanggal" value="<?=$psms->time?>" />
			<input type="hidden" name="id" value="<?=$id?>" />
		<fieldset class="inlineLabels">
			<h3>Lokasi Kejadian</h3>
			<div class="ctrlHolder">
			  <label for="">Daerah</label><ul class="alternate">
			  	<li>
					
					Provinsi
					<select name="KdProv" id="provinsi" class="selectInput required">
                      <option value="">-Pilih-</option>
                      <?php foreach($prov->result() as $row):?>
                      <option value="<?=$row->KdProv?>">
                        <?=$row->NmProv?>
                      </option>
                      <?php endforeach;?>
                    </select>

				</li>
				<li>
					
					Kabupaten / Kota
					<select id="kabkota" name="KdKab" class="selectInput required" >
                      <option value="">-Pilih-</option>
                    </select>
					
				</li>
				<li>
					
					Kecamatan
					<select id="kecamatan" name="KdKec" class="selectInput">
                      <option value="">-Pilih-</option>
                    </select>
					
				</li>
			  </ul>
			  
			  <p class="formHint">&nbsp;</p>
        	</div>
			
			<div class="ctrlHolder">
			  <label for="">Jenjang</label>
			  <select name="KdJenjang" id="jenjang" class="medium required">
						<option value="">-Pilih-</option>
						<option value="1">SD</option>
						<option value="2">SMP</option>
						<option value="3">SD & SMP</option>
						<option value="4">Lainnya</option>
			  </select>
			  <p class="formHint">&nbsp;</p>

        	</div>
			
			<div class="ctrlHolder">
			
			  <label for="" id="namasek">Nama Sekolah </label>
			  <input type="text" name="NmSekolah" class="textInput medium required" value="<?=$psms->sekolah?>"/>
			  <p class="formHint">&nbsp;</p>
        	</div>	
			</fieldset>
			<fieldset class="inlineLabels">
			<h3><label for="">Kategori Pengaduan</label></h3>
			<div class="ctrlHolder">
			  
				<label for="">Kategori</label>
				<select name="KdKategori" id="KdKategori" class="medium required KdKategori">
                  <option value="">-Pilih-</option>
                  <option value="1">Pertanyaan</option>
                  <option value="2">Saran</option>
                  <option value="3">Penyimpangan Peraturan</option>
                  <option value="4">Ketidaksesuaian Penggunaan Dana</option>
                </select>
				<p class="formHint">&nbsp;</p>
			  <p class="formHint">&nbsp;</p>
        	</div>	
			<div class="ctrlHolder" id="KetRpKat4" style="display:none">
			  <label for="">Indikasi Dana yang Disimpangkan (Rp)</label>
			  	<input type="text" name="KetRpKat4" class="textInput auto validateInteger" id="KetRpKat4_text"/>
			  <p class="formHint">&nbsp;</p>
        	</div>
			</fieldset>
		<fieldset class="inlineLabels">
			<h3>Identitas Pelapor </h3>
			<div class="ctrlHolder">
			  	<p class="label">Telp</p>
				<ul>
				<li>
				<label for=""><p style="display:none">Telp</p>
			  	<input type="text" class="textInput medium required" name="telp" value="<?=$psms->msisdn?>" readonly="readonly"/>
				</label>
				
				</li>
				
				</ul>
			  <p class="formHint">&nbsp;</p>
        	</div>
			<div class="ctrlHolder">
			<p class="label">Alamat</p>
			<ul>
			<li>
			  <label for=""><p style="display:none">Alamat</p>
			  	<textarea class="medium" name="alamat" style="height:5em"></textarea>
			  </label>
			</li>
			
			</ul>
			  <p class="formHint">&nbsp;</p>
        	</div>
				
			</fieldset>
			<fieldset class="inlineLabels">
			<h3>Deskripsi</h3>
			<div class="ctrlHolder">
			  <label for=""></label>
			  	<textarea name="Deskripsi" class="medium required " ><?=$psms->pengaduan?></textarea>
			  <p class="formHint">&nbsp;</p>
        	</div>
			
			</fieldset>
			
			<div class="buttonHolder">
        	
        	<button class="primaryAction" type="submit">Terima</button>
      </div>
</form>
<script type="text/javascript" src="<?=$this->config->item('home_js')?>/uni-form.jquery.js"></script>
	 <script type="text/javascript" src="<?=$this->config->item('home_js')?>/uni-form-validation.jquery.js"></script>
	 <script type="text/javascript">
       
	   $(function(){
        $('form.uniForm').uniform({
			prevent_submit          : true
        });
		
      });
       </script>
			
          
		  </div>
    <div id="tabs-2">
        <p><b>Dari <?=$psms->msisdn?> (<i><?=$psms->nama?></i>), <?=mysqldatetime_to_date($psms->time,"d/m/Y, H:i:s")?></b></p>
		<p>Pesan:<br><?='BOS '.$psms->sekolah.'#'.$psms->lokasi.'#'.$psms->pengaduan?></p>
    </div>
    
</div>


</div>
</div>
</body>
</html>
